﻿using MDReader.Control;
using MDReader.Extension;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips
{
    public class tool_001_MinLevel : toolBaseCtrlEditor
    {
        public tool_001_MinLevel(ILog log)
        {
            logger = log;
        }
        private ILog logger;

        public override string ShowName => "-";
        public override string Description => "减少大纲级别";
        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {

            if (me.getTxtSource != null)
            {
                try
                {
                    RichTextBox txtSource = me.getTxtSource();
                    // 获取当前选中的文本
                    string selectedText = txtSource.SelectedText;
                    string sourceSelectedText = selectedText;
                    if (!string.IsNullOrEmpty(selectedText))
                    {
                        // 如果当前选中了文本，那么给这个文本第一行的第一个字母前插入一个#号
                        int selectionStart = txtSource.SelectionStart;
                        int lineIndex = txtSource.GetLineFromCharIndex(selectionStart);
                        int firstCharIndex = txtSource.GetFirstCharIndexFromLine(lineIndex);
                        txtSource.Select(firstCharIndex, 2);
                        int offset = 0;
                        if (txtSource.SelectedText == "# ")
                        {
                            txtSource.SelectedText = "";
                            offset = -2;
                        }
                        else
                        {
                            txtSource.Select(firstCharIndex, 1);
                            if (txtSource.SelectedText == "#")
                            {
                                txtSource.SelectedText = "";
                                offset = -1;
                            }
                        }
                        txtSource.Select(selectionStart, sourceSelectedText.Length + offset);
                    }
                    else
                    {
                        // 如果当前没选中文本，那么在当前行的第一个字母前插入一个#号
                        int caretPosition = txtSource.SelectionStart;
                        int lineIndex = txtSource.GetLineFromCharIndex(caretPosition);
                        int firstCharIndex = txtSource.GetFirstCharIndexFromLine(lineIndex);
                        txtSource.Select(firstCharIndex, 2);
                        int offset = 0;
                        if (txtSource.SelectedText == "# ")
                        {
                            txtSource.SelectedText = "";
                            offset = -2;
                        }
                        else
                        {
                            txtSource.Select(firstCharIndex, 1);
                            if (txtSource.SelectedText == "#")
                            {
                                txtSource.SelectedText = "";
                                offset = -1;
                            }
                        }
                        txtSource.Select(caretPosition + offset, 0); // Move caret right after the inserted '#'
                    }
                }
                catch (Exception ex)
                { logger?.WriteExceptionToEventLog(ex); }
            }

        }
    }
}
