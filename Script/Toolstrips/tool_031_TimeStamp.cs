﻿using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips
{
    internal class tool_031_TimeStamp : toolBaseCtrlEditor
    {
        public tool_031_TimeStamp(ILog log)
        {
            logger = log;
        }
        private ILog logger;


        public override string ShowName => "时间";
        public override string Description => "插入当前日期时间";

        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {

            if (me.getTxtSource != null)
            {

                try
                {
                    RichTextBox txtSource = me.getTxtSource();
                    // 获取当前选中的文本
                    string selectedText = txtSource.SelectedText;
                    if (!string.IsNullOrEmpty(selectedText))
                    {
                        // 如果当前选中了文本，那么用下一行的线代替选中的文本
                        int selectionStart = txtSource.SelectionStart;
                        string modifiedText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        txtSource.SelectedText = modifiedText;
                        txtSource.Select(selectionStart + modifiedText.Length, 0);
                    }
                    else
                    {
                        // 如果当前没选中文本，那么下一行插一条线
                        int caretPosition = txtSource.SelectionStart;
                        txtSource.Select(caretPosition, 0);
                        string modifiedText = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        txtSource.SelectedText = modifiedText;
                        txtSource.Select(caretPosition + modifiedText.Length, 0);
                    }
                }
                catch (Exception ex)
                { logger?.WriteExceptionToEventLog(ex); }

            }

        }
    }
}
