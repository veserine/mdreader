﻿using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips
{
    public class tool_020_AddLine : toolBaseCtrlEditor
    {
        public tool_020_AddLine(ILog log)
        {
            logger = log;
        }
        private ILog logger;

        public override string ShowName => "---";
        public override string Description => "下方增加一条线";

        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {

            if (me.getTxtSource != null)
            {

                try
                {
                    RichTextBox txtSource = me.getTxtSource();
                    // 获取当前选中的文本
                    string selectedText = txtSource.SelectedText;
                    if (!string.IsNullOrEmpty(selectedText))
                    {
                        // 如果当前选中了文本，那么用下一行的线代替选中的文本
                        int selectionStart = txtSource.SelectionStart;
                        string modifiedText = $"\n\n-----------\n\n";
                        txtSource.SelectedText = modifiedText;
                        txtSource.Select(selectionStart + 2 + modifiedText.Length - 4 + 1, 0);
                    }
                    else
                    {
                        // 如果当前没选中文本，那么下一行插一条线
                        int caretPosition = txtSource.SelectionStart;
                        txtSource.Select(caretPosition, 0);
                        string modifiedText = $"\n\n-----------\n\n";
                        txtSource.SelectedText = modifiedText;
                        txtSource.Select(caretPosition + 2 + modifiedText.Length - 4 + 1, 0);
                    }
                }
                catch (Exception ex)
                { logger?.WriteExceptionToEventLog(ex); }

            }

        }
    }
}
