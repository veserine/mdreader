﻿using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips
{
    public class tool_010_Bold : toolBaseCtrlEditor
    {
        public tool_010_Bold(ILog log)
        {
            logger = log;
        }

        private ILog logger;
        public override string ShowName => "粗体";
        public override string Description => "使选中的内容增加粗体的格式";
        //protected override void ToolstripItem_ClickAfter(MDReader.Extension.IResource? e)
        //{
        //    if (e is ICtrlEditorResource me)
        //    {
        //        if (me.getTxtSource != null)
        //        {
        //            try
        //            {
        //                // 获取当前选中的文本
        //                string selectedText = me.getTxtSource().SelectedText;
        //                if (!string.IsNullOrEmpty(selectedText))
        //                {
        //                    // 如果当前选中了文本，那么给这个文本前后增加星号(*)
        //                    me.getTxtSource().SelectedText = "**" + selectedText + "**";
        //                }
        //                else
        //                {
        //                    // 如果当前没选中文本，那么在当前位置增加两个星号(*)
        //                    int selectionStart = me.getTxtSource().SelectionStart;
        //                    me.getTxtSource().Text = me.getTxtSource().Text.Insert(selectionStart, "****");
        //                    // 把光标放在两个星号中间
        //                    me.getTxtSource().SelectionStart = selectionStart + 2;
        //                }
        //            }
        //            catch (Exception ex)
        //            { logger?.WriteExceptionToEventLog(ex); }
        //        }
        //    }
        //}
        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {
            if (me.getTxtSource != null)
            {
                try
                {
                    // 获取当前选中的文本
                    string selectedText = me.getTxtSource().SelectedText;
                    if (!string.IsNullOrEmpty(selectedText) && !selectedText.Contains($"\n\n"))
                    {
                        // 如果当前选中了文本，那么给这个文本前后增加星号(*)
                        me.getTxtSource().SelectedText = "**" + selectedText + "**";
                    }
                    else if (!selectedText.Contains($"\n\n"))
                    {
                        // 如果当前没选中文本，那么在当前位置增加两个星号(*)
                        int selectionStart = me.getTxtSource().SelectionStart;
                        me.getTxtSource().Text = me.getTxtSource().Text.Insert(selectionStart, "****");
                        // 把光标放在两个星号中间
                        me.getTxtSource().SelectionStart = selectionStart + 2;
                    }
                    else // 如果跨行，不处理
                    { 
                    
                    }
                }
                catch (Exception ex)
                { logger?.WriteExceptionToEventLog(ex); }
            }
        }

    }
}
