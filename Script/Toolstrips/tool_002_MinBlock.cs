﻿using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips
{
    public class tool_002_MinBlock : toolBaseCtrlEditor
    {
        public tool_002_MinBlock(ILog log)
        {
            logger = log;
        }
        private ILog logger;

        public override string ShowName => "<";
        public override string Description => "减少文本块级别";

        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {

            if (me.getTxtSource != null)
            {

                try
                {
                    RichTextBox txtSource = me.getTxtSource();
                    // 获取当前选中的文本
                    string selectedText = txtSource.SelectedText;
                    string sourceSelectedText = selectedText;
                    if (!string.IsNullOrEmpty(selectedText))
                    {
                        // 如果当前选中了文本，那么给这个文本每一行的第一个字母前插入一个>号
                        int selectionStart = txtSource.SelectionStart;
                        int selectionLength = selectedText.Length;
                        int lineIndex = txtSource.GetLineFromCharIndex(selectionStart);
                        int firstCharIndex = txtSource.GetFirstCharIndexFromLine(lineIndex);
                        StringBuilder modifiedText = new StringBuilder();
                        // Loop through each line in the selected text
                        string[] lines = selectedText.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.None);
                        foreach (string line in lines)
                        {
                            if (line.StartsWith('>'))
                            {
                                modifiedText.Append(line.Substring(1, line.Length - 1) + Environment.NewLine);
                            }
                        }
                        // Remove the last newline character if the selected text doesn't end with a newline
                        //if (!selectedText.EndsWith(Environment.NewLine))
                        //{
                        //    modifiedText.Length -= Environment.NewLine.Length;
                        //}
                        // Insert the modified text back into the RichTextBox
                        txtSource.Select(firstCharIndex, selectionLength);
                        txtSource.SelectedText = modifiedText.ToString();
                        txtSource.Select(selectionStart, modifiedText.Length);
                    }
                    else
                    {
                        // 如果当前没选中文本，那么在当前行的第一个字母前插入一个>号
                        int caretPosition = txtSource.SelectionStart;
                        int lineIndex = txtSource.GetLineFromCharIndex(caretPosition);
                        int firstCharIndex = txtSource.GetFirstCharIndexFromLine(lineIndex);
                        txtSource.Select(firstCharIndex, 1);
                        if (txtSource.SelectedText == ">")
                        {
                            txtSource.SelectedText = "";
                        }
                        txtSource.Select(caretPosition - 1, 0); // Move caret right after the inserted '>'
                    }
                }
                catch (Exception ex)
                { logger?.WriteExceptionToEventLog(ex); }

            }

        }
    }
}
