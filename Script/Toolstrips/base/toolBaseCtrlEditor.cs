﻿using MDReader.Control;
using MDReader.Extension;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Toolstrips.Base
{
    public abstract class toolBaseCtrlEditor : ToolstripItemTemplate
    {
        protected override void ToolstripItem_ClickAfter(IResource? e)
        {
            if (e is ICtrlEditorResource me)
                CtrlEditorToolstripItem_ClickAfter(me);
            else
                base.ToolstripItem_ClickAfter(e);
        }
        protected virtual void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        { 
        
        }
    }
}
