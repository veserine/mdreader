﻿using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Toolstrips.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MDReader.Script.Toolstrips.Base
{
    public class tool_022_AddTable : toolBaseCtrlEditor
    {
        public tool_022_AddTable(ILog log)
        {
            logger = log;
        }
        private ILog logger;

        public override string ShowName => "表格";
        public override string Description => "插入一个表格";


        protected override void CtrlEditorToolstripItem_ClickAfter(ICtrlEditorResource me)
        {
            try
            {
                if (me.getTxtSource == null)
                    return;
                RichTextBox txtSource = me.getTxtSource();
                // 弹出对话框询问用户想要的表格的行数和列数
                using (Form inputForm = new Form())
                {
                    Label labelRows = new Label() { Text = "请输入行数:", AutoSize = true };
                    Label labelCols = new Label() { Text = "请输入列数:", AutoSize = true };
                    NumericUpDown numericUpDownRows = new NumericUpDown() { Minimum = 1, Value = 5 };
                    NumericUpDown numericUpDownCols = new NumericUpDown() { Minimum = 1, Value = 3 };
                    Button buttonOk = new Button() { Text = "确定", DialogResult = DialogResult.OK };
                    buttonOk.Click += (sender, e) => { inputForm.DialogResult = DialogResult.OK; };
                    labelRows.SetBounds(9, 20, 372, 13);
                    numericUpDownRows.SetBounds(12, 36, 120, 20);
                    labelCols.SetBounds(9, 64, 372, 13);
                    numericUpDownCols.SetBounds(12, 80, 120, 20);
                    buttonOk.SetBounds(228, 36, 75, 23);
                    buttonOk.Dock = DockStyle.Fill;
                    Panel pnl = new Panel();
                    pnl.Dock = DockStyle.Bottom;
                    pnl.Height = 35;
                    Panel pnlOk = new Panel();
                    pnlOk.Dock = DockStyle.Right;
                    pnlOk.Width = 80;
                    pnlOk.Padding = new Padding(5);
                    pnlOk.Controls.Add(buttonOk);
                    pnl.Controls.Add(pnlOk);
                    //if (me.getMyself != null)
                    //    inputForm.Parent = me.getMyself();
                    inputForm.Size = new Size(180, 220);
                    inputForm.StartPosition = FormStartPosition.CenterParent;
                    inputForm.Controls.AddRange(new System.Windows.Forms.Control[] { labelRows, numericUpDownRows, labelCols, numericUpDownCols, pnl });
                    inputForm.AcceptButton = buttonOk;
                    inputForm.Text = "插入表格";
                    if (inputForm.ShowDialog() == DialogResult.OK)
                    {
                        int rows = (int)numericUpDownRows.Value;
                        int cols = (int)numericUpDownCols.Value;
                        // 生成Markdown表格
                        StringBuilder sb = new StringBuilder();
                        string selectedText = txtSource.SelectedText;
                        sb.Append("\n\n");
                        string[] lines = selectedText.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);
                        for (int i = 0; i < rows; i++)
                        {
                            sb.Append("|");
                            for (int j = 0; j < cols; j++)
                            {
                                // 如果有选中的文本并且当前行存在，则填充文本，否则填充空格
                                if (i < lines.Length && j == 0 && !string.IsNullOrEmpty(lines[i].Replace(" ", "")))
                                {
                                    string line = lines[i].Replace(" ", "");
                                    sb.Append(" " + line + " ");
                                }
                                else if (i < lines.Length && i == 0 && string.IsNullOrEmpty(lines[i].Replace(" ", "")))
                                {
                                    string line = lines[i].Replace(" ", "");
                                    sb.Append(" **标题** ");
                                }
                                else
                                {
                                    sb.Append("              ");
                                }
                                sb.Append("|");
                            }
                            sb.AppendLine();
                            if (i == 0)
                            {
                                // 添加分割线
                                sb.Append("|");
                                for (int j = 0; j < cols; j++)
                                {
                                    sb.Append("-----------|");
                                }
                                sb.AppendLine();
                            }
                        }
                        sb.Append("\n\n");
                        // 插入生成的表格到当前光标位置
                        txtSource.SelectedText = sb.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger?.WriteExceptionToEventLog(ex);
            }


        }
    }
}
