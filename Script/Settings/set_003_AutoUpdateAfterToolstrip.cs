﻿using MDReader.Helper;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Settings
{
    public class set_003_AutoUpdateAfterToolstrip : SettingTemplate
    {
        public set_003_AutoUpdateAfterToolstrip(ILog log, IConfigurationService configurationService) : base(log, configurationService)
        {
        }
        public override string SettingName => "使用编辑工具后自动更新";
        public override string DefaultValue => "false";
        public override ISettingTypes SettingType => ISettingTypes.Check;
    }
}
