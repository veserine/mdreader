﻿using MDReader.Helper;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Settings
{
    internal class set_000_SyncEditor : SettingTemplate
    {
        public set_000_SyncEditor(ILog log, IConfigurationService configurationService) : base(log, configurationService)
        {
        }

        public override string SettingName => "源码随页面滚动";
        public override string DefaultValue => "false";
        public override ISettingTypes SettingType => ISettingTypes.Check;
    }
}
