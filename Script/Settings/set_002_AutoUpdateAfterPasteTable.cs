﻿using MDReader.Helper;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Settings
{
    public class set_002_AutoUpdateAfterPasteTable : SettingTemplate
    {
        public set_002_AutoUpdateAfterPasteTable(ILog log, IConfigurationService configurationService) : base(log, configurationService)
        {
        }
        public override string SettingName => "粘贴表格时自动更新";
        public override string DefaultValue => "false";
        public override ISettingTypes SettingType => ISettingTypes.Check;
    }
}
