﻿using MDReader.Helper;
using MDReader.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Script.Settings
{
    internal class set_001_SyncBrowser : SettingTemplate
    {
        public set_001_SyncBrowser(ILog log, IConfigurationService configurationService) : base(log, configurationService)
        {
        }
        public override string SettingName => "页面随源码滚动";
        public override string DefaultValue => "false";
        public override ISettingTypes SettingType => ISettingTypes.Check;
    }
}
