# MDReader

#### 介绍
MD文件阅读器

#### 软件架构
软件架构说明


#### 安装教程

1.  依赖Microsoft Edge WebView2组件，需要先在 https://developer.microsoft.com/zh-cn/microsoft-edge/webview2?form=MA13LH#download 处下载并安装

#### 使用说明

1.  阅读功能
2.  编辑原文功能（仅编辑原文，不能所见即所得）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
