﻿namespace MDReader
{
    partial class frmReader
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            菜单 = new MenuStrip();
            文件FToolStripMenuItem = new ToolStripMenuItem();
            新建NToolStripMenuItem = new ToolStripMenuItem();
            打开OToolStripMenuItem = new ToolStripMenuItem();
            保存SToolStripMenuItem = new ToolStripMenuItem();
            关闭CToolStripMenuItem = new ToolStripMenuItem();
            toolStripSeparator1 = new ToolStripSeparator();
            退出QToolStripMenuItem = new ToolStripMenuItem();
            编辑EToolStripMenuItem = new ToolStripMenuItem();
            查找FToolStripMenuItem = new ToolStripMenuItem();
            修改CToolStripMenuItem1 = new ToolStripMenuItem();
            显示VToolStripMenuItem = new ToolStripMenuItem();
            大纲MToolStripMenuItem = new ToolStripMenuItem();
            选项OToolStripMenuItem = new ToolStripMenuItem();
            设置SToolStripMenuItem = new ToolStripMenuItem();
            打开文件框 = new OpenFileDialog();
            statusStrip1 = new StatusStrip();
            tsslOpening = new ToolStripStatusLabel();
            tspbOpening = new ToolStripProgressBar();
            cBrowser = new Control.CtrlBrowser();
            splitOutLine = new SplitContainer();
            cOutline = new Control.CtrlOutline();
            splitEditor = new SplitContainer();
            gbSource = new GroupBox();
            cEditor = new Control.CtrlEditor();
            保存文件框 = new SaveFileDialog();
            菜单.SuspendLayout();
            statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitOutLine).BeginInit();
            splitOutLine.Panel1.SuspendLayout();
            splitOutLine.Panel2.SuspendLayout();
            splitOutLine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitEditor).BeginInit();
            splitEditor.Panel1.SuspendLayout();
            splitEditor.Panel2.SuspendLayout();
            splitEditor.SuspendLayout();
            gbSource.SuspendLayout();
            SuspendLayout();
            // 
            // 菜单
            // 
            菜单.Enabled = false;
            菜单.Items.AddRange(new ToolStripItem[] { 文件FToolStripMenuItem, 编辑EToolStripMenuItem, 选项OToolStripMenuItem });
            菜单.Location = new Point(0, 0);
            菜单.Name = "菜单";
            菜单.Size = new Size(800, 25);
            菜单.TabIndex = 0;
            菜单.Text = "menuStrip1";
            // 
            // 文件FToolStripMenuItem
            // 
            文件FToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 新建NToolStripMenuItem, 打开OToolStripMenuItem, 保存SToolStripMenuItem, 关闭CToolStripMenuItem, toolStripSeparator1, 退出QToolStripMenuItem });
            文件FToolStripMenuItem.Name = "文件FToolStripMenuItem";
            文件FToolStripMenuItem.Size = new Size(58, 21);
            文件FToolStripMenuItem.Text = "文件(&F)";
            // 
            // 新建NToolStripMenuItem
            // 
            新建NToolStripMenuItem.Name = "新建NToolStripMenuItem";
            新建NToolStripMenuItem.Size = new Size(165, 22);
            新建NToolStripMenuItem.Text = "新建(&N)";
            新建NToolStripMenuItem.Click += 新建NToolStripMenuItem_Click;
            // 
            // 打开OToolStripMenuItem
            // 
            打开OToolStripMenuItem.Name = "打开OToolStripMenuItem";
            打开OToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.O;
            打开OToolStripMenuItem.Size = new Size(165, 22);
            打开OToolStripMenuItem.Text = "打开(&O)";
            打开OToolStripMenuItem.Click += 打开OToolStripMenuItem_Click;
            // 
            // 保存SToolStripMenuItem
            // 
            保存SToolStripMenuItem.Enabled = false;
            保存SToolStripMenuItem.Name = "保存SToolStripMenuItem";
            保存SToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.S;
            保存SToolStripMenuItem.Size = new Size(165, 22);
            保存SToolStripMenuItem.Text = "保存(&S)";
            保存SToolStripMenuItem.Click += 保存SToolStripMenuItem_Click;
            // 
            // 关闭CToolStripMenuItem
            // 
            关闭CToolStripMenuItem.Name = "关闭CToolStripMenuItem";
            关闭CToolStripMenuItem.Size = new Size(165, 22);
            关闭CToolStripMenuItem.Text = "关闭(&C)";
            关闭CToolStripMenuItem.Click += 关闭CToolStripMenuItem_Click;
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new Size(162, 6);
            // 
            // 退出QToolStripMenuItem
            // 
            退出QToolStripMenuItem.Name = "退出QToolStripMenuItem";
            退出QToolStripMenuItem.Size = new Size(165, 22);
            退出QToolStripMenuItem.Text = "退出(&Q)";
            退出QToolStripMenuItem.Click += 退出QToolStripMenuItem_Click;
            // 
            // 编辑EToolStripMenuItem
            // 
            编辑EToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 查找FToolStripMenuItem, 修改CToolStripMenuItem1, 显示VToolStripMenuItem });
            编辑EToolStripMenuItem.Name = "编辑EToolStripMenuItem";
            编辑EToolStripMenuItem.Size = new Size(59, 21);
            编辑EToolStripMenuItem.Text = "编辑(&E)";
            // 
            // 查找FToolStripMenuItem
            // 
            查找FToolStripMenuItem.Enabled = false;
            查找FToolStripMenuItem.Name = "查找FToolStripMenuItem";
            查找FToolStripMenuItem.ShortcutKeys = Keys.Control | Keys.F;
            查找FToolStripMenuItem.Size = new Size(160, 22);
            查找FToolStripMenuItem.Text = "查找(&F)";
            查找FToolStripMenuItem.Visible = false;
            查找FToolStripMenuItem.Click += 查找FToolStripMenuItem_Click;
            // 
            // 修改CToolStripMenuItem1
            // 
            修改CToolStripMenuItem1.Name = "修改CToolStripMenuItem1";
            修改CToolStripMenuItem1.ShortcutKeys = Keys.Control | Keys.E;
            修改CToolStripMenuItem1.Size = new Size(160, 22);
            修改CToolStripMenuItem1.Text = "修改(&C)";
            修改CToolStripMenuItem1.Click += 修改CToolStripMenuItem_Click;
            // 
            // 显示VToolStripMenuItem
            // 
            显示VToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 大纲MToolStripMenuItem });
            显示VToolStripMenuItem.Name = "显示VToolStripMenuItem";
            显示VToolStripMenuItem.Size = new Size(160, 22);
            显示VToolStripMenuItem.Text = "显示(&V)";
            // 
            // 大纲MToolStripMenuItem
            // 
            大纲MToolStripMenuItem.Name = "大纲MToolStripMenuItem";
            大纲MToolStripMenuItem.Size = new Size(120, 22);
            大纲MToolStripMenuItem.Text = "大纲(&M)";
            大纲MToolStripMenuItem.Click += 大纲MToolStripMenuItem_Click;
            // 
            // 选项OToolStripMenuItem
            // 
            选项OToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 设置SToolStripMenuItem });
            选项OToolStripMenuItem.Name = "选项OToolStripMenuItem";
            选项OToolStripMenuItem.Size = new Size(62, 21);
            选项OToolStripMenuItem.Text = "选项(&O)";
            // 
            // 设置SToolStripMenuItem
            // 
            设置SToolStripMenuItem.Name = "设置SToolStripMenuItem";
            设置SToolStripMenuItem.Size = new Size(115, 22);
            设置SToolStripMenuItem.Text = "设置(&S)";
            设置SToolStripMenuItem.Click += 设置SToolStripMenuItem_Click;
            // 
            // 打开文件框
            // 
            打开文件框.DefaultExt = "md";
            打开文件框.FileName = "*.md";
            打开文件框.Title = "打开MarkDown文件";
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { tsslOpening, tspbOpening });
            statusStrip1.Location = new Point(0, 428);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(800, 22);
            statusStrip1.TabIndex = 3;
            statusStrip1.Text = "statusStrip1";
            // 
            // tsslOpening
            // 
            tsslOpening.Name = "tsslOpening";
            tsslOpening.Size = new Size(56, 17);
            tsslOpening.Text = "正在加载";
            // 
            // tspbOpening
            // 
            tspbOpening.MarqueeAnimationSpeed = 30;
            tspbOpening.Name = "tspbOpening";
            tspbOpening.Size = new Size(250, 16);
            tspbOpening.Style = ProgressBarStyle.Marquee;
            // 
            // cBrowser
            // 
            cBrowser.Dock = DockStyle.Fill;
            cBrowser.Location = new Point(0, 0);
            cBrowser.Name = "cBrowser";
            cBrowser.Size = new Size(266, 403);
            cBrowser.TabIndex = 4;
            cBrowser.InitalizedAfter += cBrowser_InitalizedAfter;
            cBrowser.ReceivedAfter += cBrowser_ReceivedAfter;
            // 
            // splitOutLine
            // 
            splitOutLine.Dock = DockStyle.Fill;
            splitOutLine.Location = new Point(0, 25);
            splitOutLine.Name = "splitOutLine";
            // 
            // splitOutLine.Panel1
            // 
            splitOutLine.Panel1.Controls.Add(cOutline);
            splitOutLine.Panel1Collapsed = true;
            // 
            // splitOutLine.Panel2
            // 
            splitOutLine.Panel2.Controls.Add(splitEditor);
            splitOutLine.Size = new Size(800, 403);
            splitOutLine.SplitterDistance = 130;
            splitOutLine.TabIndex = 5;
            // 
            // cOutline
            // 
            cOutline.Dock = DockStyle.Fill;
            cOutline.Location = new Point(0, 0);
            cOutline.Name = "cOutline";
            cOutline.Size = new Size(130, 100);
            cOutline.TabIndex = 0;
            cOutline.OutlineItemSelected += OutlineForm_OutlineItemSelected;
            // 
            // splitEditor
            // 
            splitEditor.Dock = DockStyle.Fill;
            splitEditor.Location = new Point(0, 0);
            splitEditor.Name = "splitEditor";
            // 
            // splitEditor.Panel1
            // 
            splitEditor.Panel1.Controls.Add(cBrowser);
            // 
            // splitEditor.Panel2
            // 
            splitEditor.Panel2.Controls.Add(gbSource);
            splitEditor.Size = new Size(800, 403);
            splitEditor.SplitterDistance = 266;
            splitEditor.TabIndex = 5;
            // 
            // gbSource
            // 
            gbSource.Controls.Add(cEditor);
            gbSource.Dock = DockStyle.Fill;
            gbSource.Location = new Point(0, 0);
            gbSource.Name = "gbSource";
            gbSource.Size = new Size(530, 403);
            gbSource.TabIndex = 5;
            gbSource.TabStop = false;
            gbSource.Text = "md原文";
            // 
            // cEditor
            // 
            cEditor.Content = "";
            cEditor.Dock = DockStyle.Fill;
            cEditor.Location = new Point(3, 19);
            cEditor.Name = "cEditor";
            cEditor.Size = new Size(524, 381);
            cEditor.TabIndex = 0;
            cEditor.Accepted += cEditor_Accepted;
            cEditor.Canceled += cEditor_Canceled;
            cEditor.Pasted += cEditor_Pasted;
            cEditor.Toolstripped += cEditor_Toolstripped;
            cEditor.SyncBrowserToOutline += cEditor_SyncBrowserToOutline;
            // 
            // 保存文件框
            // 
            保存文件框.AddToRecent = false;
            保存文件框.DefaultExt = "md";
            保存文件框.FileName = "*.md";
            保存文件框.Title = "保存MarkDown文件";
            // 
            // frmReader
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitOutLine);
            Controls.Add(statusStrip1);
            Controls.Add(菜单);
            MainMenuStrip = 菜单;
            Name = "frmReader";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "MarkDown阅读器";
            FormClosing += frmReader_FormClosing;
            Load += frmReader_Load;
            菜单.ResumeLayout(false);
            菜单.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            splitOutLine.Panel1.ResumeLayout(false);
            splitOutLine.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitOutLine).EndInit();
            splitOutLine.ResumeLayout(false);
            splitEditor.Panel1.ResumeLayout(false);
            splitEditor.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitEditor).EndInit();
            splitEditor.ResumeLayout(false);
            gbSource.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip 菜单;
        private ToolStripMenuItem 文件FToolStripMenuItem;
        private ToolStripMenuItem 打开OToolStripMenuItem;
        private ToolStripMenuItem 关闭CToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem 退出QToolStripMenuItem;
        private OpenFileDialog 打开文件框;
        private ToolStripMenuItem 编辑EToolStripMenuItem;
        private ToolStripMenuItem 查找FToolStripMenuItem;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel tsslOpening;
        private ToolStripProgressBar tspbOpening;
        private ToolStripMenuItem 显示VToolStripMenuItem;
        private ToolStripMenuItem 大纲MToolStripMenuItem;
        private Control.CtrlBrowser cBrowser;
        private SplitContainer splitOutLine;
        private Control.CtrlOutline cOutline;
        private ToolStripMenuItem 选项OToolStripMenuItem;
        private ToolStripMenuItem 设置SToolStripMenuItem;
        private SplitContainer splitEditor;
        private Control.CtrlEditor cEditor;
        private GroupBox gbSource;
        private ToolStripMenuItem 保存SToolStripMenuItem;
        private ToolStripMenuItem 修改CToolStripMenuItem1;
        private ToolStripMenuItem 新建NToolStripMenuItem;
        private SaveFileDialog 保存文件框;
    }
}
