﻿using MDReader.Control;
using MDReader.Extension;
using MDReader.Helper;
using MDReader.Properties;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Services
{
    public interface IToolstripService
    {
        event EventHandler? Toolstripped;
        MDReader.Extension.IResource? Resource { set; }
        ToolStripItem[] MakeTools();
        void Register(ServiceProvider ServiceProvider);
    }
    public class ToolstripService : IToolstripService
    {
        public ToolstripService(ILog log)
        {
            logger = log;
        }

        private ILog logger;
        public event EventHandler? Toolstripped;
        public MDReader.Extension.IResource? Resource { private get; set; }
        List<IToolstripItem> tools = new List<IToolstripItem>();

        public void Register(ServiceProvider ServiceProvider)
        {
            var settingTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(t => typeof(IToolstripItem).IsAssignableFrom(t) && t.IsClass && !t.IsAbstract);
            foreach (var settingType in settingTypes)
            {
                try
                {
                    var settingInstance = (IToolstripItem)ServiceProvider.GetRequiredKeyedService(settingType, null);
                    {
                        tools.Add(settingInstance);
                    }
                }
                catch (Exception ex)
                {
                    logger.WriteExceptionToEventLog(ex);
                }
            }
        }
        public ToolStripItem[] MakeTools()
        {
            return tools.Select(t => t.MakeTool(Toolstripped, Resource)).ToArray();
        }
    }

    public interface IToolstripItem
    {
        ToolStripItem MakeTool(EventHandler? toolstripped, MDReader.Extension.IResource? resource);
        string ShowName => "";
        string Description => "";
    }
    public abstract class ToolstripItemTemplate : IToolstripItem
    {
        public virtual string ShowName => "";
        public virtual string Description => "";
        public event EventHandler? ToolstripAfter;
        protected MDReader.Extension.IResource? Resource { get; set; }
        public ToolStripItem MakeTool(EventHandler? toolstripped, MDReader.Extension.IResource? resource)
        {
            ToolstripAfter -= toolstripped;
            ToolstripAfter += toolstripped;
            Resource = resource;
            // 创建一个ToolStripItem item
            ToolStripItem item = new ToolStripButton(); // 你可以根据需要选择不同的ToolStripItem类型，例如ToolStripButton, ToolStripMenuItem等
            item.Text = ShowName;
            item.ToolTipText = Description;
            item.Click += Item_Click;
            return item;
        }
        protected virtual void ToolstripItem_ClickAfter(MDReader.Extension.IResource? e)
        {

        }
        private void Item_Click(object? sender, EventArgs e)
        {
            ToolstripItem_ClickAfter(Resource);
            if (ToolstripAfter != null)
                ToolstripAfter(sender, e);
        }
    }
}
