﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Helper
{
    public interface ILog
    {
        void WriteToEventLog(string message, EventLogEntryType type);
        void WriteExceptionToEventLog(Exception ex);
    }
    public class LogService: ILog
    {
        public LogService(string ApplicationName = "")
        {
            if (string.IsNullOrEmpty(ApplicationName))
                ApplicationName = Application.ProductName ?? "";
            this.ApplicationName = ApplicationName;
        }

        public string ApplicationName { get; private set; }
        
        public void WriteToEventLog(string message, EventLogEntryType type)
        {
            string log = "Application";
            if (!EventLog.SourceExists(ApplicationName))
            {
                EventLog.CreateEventSource(ApplicationName, log);
            }
            EventLog.WriteEntry(ApplicationName, message, type);
        }
        public void WriteExceptionToEventLog(Exception ex)
        {
            string message = $@"{ex.Message}{System.Environment.NewLine}堆栈跟踪：{System.Environment.NewLine}{ex.StackTrace}";
            WriteToEventLog(message,  EventLogEntryType.Error);
        }
    }

}
