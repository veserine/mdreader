﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Helper
{

    public interface IMainArgs : IEnumerable<string>
    {
        void Add(string s);
        bool RemoveAt(int index);
        bool Remove(string s);
        void AddRange(string[] strings);
        string this[int index] { get; set; }
    }
    internal class MainArgsService : IMainArgs
    {
        public MainArgsService(string[]? args)
        {
            AddRange(args);
        }
        private MainArgStringEnumerator e = new MainArgStringEnumerator();
        public IEnumerator<string> GetEnumerator()
        {
            return e;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return e;
        }
        public void Add(string s)
        {
            e.Add(s);
        }

        public bool RemoveAt(int index)
        {
            return e.RemoveAt(index);
        }

        public bool Remove(string s)
        {
            return e.Remove(s);
        }

        public void AddRange(string[] strings)
        {
            e.AddRange(strings);
        }

        public string this[int index]
        {
            get
            {
                return e[index];
            }
            set
            {
                e[index] = value;
            }
        }

        internal class MainArgStringEnumerator : IEnumerator<string>
        {
            private List<string>? stringList = new List<string>();
            private int index = 0;
            public string Current
            {
                get
                {
                    if (stringList == null)
                        return string.Empty;
                    if (stringList.Count <= index)
                        index = stringList.Count - 1;
                    return stringList[index];
                }
            }

            object IEnumerator.Current => Current;

            public void Add(string s)
            {
                if (stringList == null)
                    stringList = new List<string>();
                stringList.Add(s);
            }

            public bool RemoveAt(int index)
            {
                if (stringList == null)
                    return false;
                if (stringList.Count <= index)
                    return false;
                stringList.RemoveAt(index);
                return true;
            }

            public bool Remove(string s)
            {
                if (stringList == null)
                    return false;
                if (!stringList.Contains(s))
                    return false;
                return stringList.Remove(s);
            }

            public void AddRange(string[]? strings)
            {
                if (stringList == null)
                    stringList = new List<string>();
                if (strings != null)
                    stringList.AddRange(strings);
            }

            public void Dispose()
            {
                stringList = null;
            }

            public bool MoveNext()
            {
                if (stringList == null)
                    return false;
                if (stringList.Count <= index)
                    return false;
                index++;
                return true;
            }

            public void Reset()
            {
                index = 0;
            }

            public string this[int index]
            {
                get
                {
                    if (stringList != null && stringList.Count > index)
                        return stringList[index];
                    return string.Empty;
                }
                set
                {
                    if (stringList != null && stringList.Count > index)
                        stringList[index] = value;

                }
            }

        }
    }
}
