using System.ComponentModel;
using System.Net.Http;
using System.Windows.Forms;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text;
//using iTextSharp;
//using SautinSoft;
using System.Xml;
using Markdig;
using Markdig.Syntax.Inlines;
using Markdig.Syntax;
using MDReader.Control;
using System.Reflection.Metadata;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrayNotify;
using System.Drawing;
using Markdig.Renderers.Html;
using Markdig.Renderers;
using MDReader.Extension;
using MDReader.Helper;
using Microsoft.Extensions.DependencyInjection;
using System;
using MDReader.Services;
namespace MDReader
{
    public partial class frmReader : Form
    {
        #region 构造函数
        public frmReader(ILog log, IMainArgs args, ISettingService setting, IToolstripService toolstrip)
        {
            this.logger = log;
            this.mainArg = args;
            this.settingService = setting;
            this.toolstripService = toolstrip;
            InitializeComponent();
            cEditor.Initalize(log, toolstrip);
            string filePath = "";
            foreach (string path in mainArg)
            {
                if (System.IO.File.Exists(path))
                {
                    filePath = path;
                    break;
                }
            }
            if (!string.IsNullOrEmpty(filePath))
            {
                CurrentOpenFile = filePath;
                OpenFile(filePath, false);
            }
            this.splitEditor.Panel2Collapsed = true;
        }
        #endregion

        #region 属性和字段
        private readonly ILog logger;
        private readonly IMainArgs mainArg;
        private readonly ISettingService settingService;
        private readonly IToolstripService toolstripService;
        private string ReadCache { get; set; } = "";
        //private string selectedElementId = ""; // 选中的元素序号
        //private Dictionary<string, string> markdownSourceMap = new Dictionary<string, string>(); // 元素地图
        //private int elementCounter = 0; // 元素数量
        private string CurrentOpenFile { get; set; } = "";
        private List<OutlineItem> OutlineItems { get; set; } = new List<OutlineItem>();
        private bool isSyncingToBrowser = false;
        private bool isSyncingToTextBox = false;

        private static int ReceivedCounts = 0;

        #endregion

        #region 自定义方法
        private void PutToRtf()
        {
            BackgroundWorker bg = new BackgroundWorker();
            bg.DoWork += (s, e) =>
            {
                try
                {

                    var pipeline = new MarkdownPipelineBuilder()
                        .UseAdvancedExtensions()
                        .Use(new HeadingIdExtension())
                        .Build();
                    //var pipeline = new MarkdownPipelineBuilder().UseAdvancedExtensions().Build();
                    var document = Markdown.Parse(ReadCache, pipeline);
                    string htmlText = Markdown.ToHtml(document, pipeline);
                    string completeHtml = $@"
<!DOCTYPE html>
<html lang=""en"">
<head>
    <meta charset=""UTF-8"">
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Markdown to HTML</title>
    <style>
        body {{
            font-family: Arial, sans-serif;
            margin: 20px;
        }}
        table {{
            width: 100%;
            border-collapse: collapse;
            margin: 20px 0;
        }}
        table, th, td {{
            border: 1px solid #ddd;
        }}
        th, td {{
            padding: 12px;
            text-align: left;
        }}
        th {{
            background-color: #f2f2f2;
        }}
        tr:nth-child(even) {{
            background-color: #f9f9f9;
        }}
        tr:hover {{
            background-color: #f1f1f1;
        }}


        pre {{
            position: relative;
            padding: 1em;
            background-color: #f5f5f5;
            border: 1px solid #ddd;
            margin-bottom: 1em;
        }}
        .copy-button {{
            position: absolute;
            top: 10px;
            right: 10px;
            padding: 5px 10px;
            background-color: #6c757d; /* 灰色 */
            color: white;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }}
        .copy-button:hover {{
            background-color: #5a6268; /* 深灰色 */
        }}
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', (event) => {{
            // 大纲跳转
            const links = document.querySelectorAll('a');
            links.forEach(link => {{
                link.setAttribute('target', '_blank');
            }});

            // 代码块复制按钮
            const preElements = document.querySelectorAll(""pre"");
            preElements.forEach(preElement => {{
                // Create the copy button
                const copyButton = document.createElement(""button"");
                copyButton.textContent = ""复制"";
                copyButton.className = ""copy-button"";
                // Append the button to the <pre> element
                preElement.appendChild(copyButton);
                // Add click event listener to the button
                copyButton.addEventListener(""click"", function() {{
                    // Get the code content
                    const codeContent = preElement.querySelector(""code"").textContent;
                    // Create a temporary textarea element to hold the code content
                    const tempTextArea = document.createElement(""textarea"");
                    tempTextArea.value = codeContent;
                    document.body.appendChild(tempTextArea);
                    // Select the text in the textarea and copy it to the clipboard
                    tempTextArea.select();
                    document.execCommand(""copy"");
                    // Remove the temporary textarea element
                    document.body.removeChild(tempTextArea);
                    // Optionally, provide feedback to the user
                    copyButton.textContent = ""已复制!"";
                    setTimeout(() => {{
                        copyButton.textContent = ""复制"";
                    }}, 2000);
                }});
            }});

        }});
        document.addEventListener('scroll', function() {{
            var headings = document.querySelectorAll('h1, h2, h3, h4, h5, h6');
            var currentHeading = null;
            for (var i = 0; i < headings.length; i++) {{
                var rect = headings[i].getBoundingClientRect();
                if (rect.top >= 0) {{
                    currentHeading = headings[i];
                    break;
                }}
            }}
            if (currentHeading) {{
                window.chrome.webview.postMessage(currentHeading.id);
            }}
        }});
    </script>

</head>
<body>
    {htmlText}
</body>
</html>";
                    e.Result = new Tuple<string, MarkdownDocument, MarkdownPipeline>(completeHtml, document, pipeline);

                }
                catch (Exception ex)
                {
                    e.Result = ex;
                }
            };
            bg.RunWorkerCompleted += (s, e) =>
            {
                if (e.Result != null && e.Result is Exception)
                {
                    Exception ex = e.Result as Exception;
                    MessageBox.Show($"解析文件时遇到问题: {ex.Message}");
                }
                else if (e.Result != null && e.Result is Tuple<string, MarkdownDocument, MarkdownPipeline>)
                {
                    Tuple<string, MarkdownDocument, MarkdownPipeline>? Result = e.Result as Tuple<string, MarkdownDocument, MarkdownPipeline>;
                    if (Result != null)
                    {
                        MarkdownDocument document = Result.Item2;
                        MarkdownPipeline pipeline = Result.Item3;
                        string htmlText = Result.Item1;
                        try
                        {
                            // 生成内容
                            cBrowser.NavigateTo(htmlText);

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"显示文件时遇到问题: {ex.Message}");
                        }
                        try
                        {
                            // 生成大纲
                            var outlineItems = GenerateOutline(document);
                            cOutline.LoadOutline(outlineItems);
                            OutlineItems = outlineItems;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"生成大纲时遇到问题: {ex.Message}");
                        }
                        try
                        {
                            // 准备编辑
                            cEditor.Content = ReadCache;

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"准备编辑时遇到问题: {ex.Message}");
                        }
                    }

                }
                this.文件FToolStripMenuItem.Enabled = true;
                tsslOpening.Visible = false;
                tspbOpening.Visible = false;
                this.tspbOpening.Style = ProgressBarStyle.Continuous;
                this.tspbOpening.MarqueeAnimationSpeed = 30; // 设置滚动速度，值越小速度越快
            };
            bg.RunWorkerAsync();
            this.文件FToolStripMenuItem.Enabled = false;
            this.tsslOpening.Visible = true;
            this.tspbOpening.Visible = true;
            this.tspbOpening.Style = ProgressBarStyle.Marquee;
            this.tspbOpening.MarqueeAnimationSpeed = 30; // 设置滚动速度，值越小速度越快
        }

        private List<OutlineItem> GenerateOutline(MarkdownDocument document)
        {
            var outlineItems = new List<OutlineItem>();
            foreach (var node in document.Descendants<HeadingBlock>())
            {
                HeadingBlock? heading = node as HeadingBlock;
                if (heading != null && heading.Inline != null)
                {
                    var text = string.Join("", heading.Inline.Select(x => x.ToString()));
                    outlineItems.Add(new OutlineItem
                    {
                        Level = heading.Level,
                        Text = text,
                        Id = $"heading-{outlineItems.Count}",
                        LineNumber = heading.Line // 记录行号
                    });
                }
            }
            return outlineItems;
        }

        private void OpenFile(string filePath, bool Now = true)
        {
            try
            {
                // 读取文件内容到字符串
                ReadCache = File.ReadAllText(filePath);

                if (Now)
                    PutToRtf();

                string fileName = Path.GetFileName(filePath);

                this.Text = $"MD阅读器      {fileName}";
                保存SToolStripMenuItem.Enabled = true;
            }
            catch (Exception ex)
            {
                // 处理可能的异常，例如文件读取错误
                MessageBox.Show($"读取时遇到问题: {ex.Message}");
            }
        }

        private void ShowMarkdownEditor(string markdownSource)
        {
            //markdownTextBox.Text = markdownSource;
            //markdownTextBox.Visible = true;
            //applyButton.Visible = true;
            //CtrlEditor editor = new CtrlEditor() { Content = markdownSource };
            //if (editor.ShowDialog() == DialogResult.OK) 
            //{

            //}
        }

        #endregion

        #region 重写方法
        #endregion

        #region 效果实现
        private void OutlineForm_OutlineItemSelected(object? sender, OutlineItemSelectedEventArgs e)
        {
            try
            {
                cBrowser.JumpTo(e.Item.Id);
            }
            catch { }
        }
        private void 查找FToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cBrowser.ShowFind();
        }

        private void 退出QToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void 关闭CToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //markdownSourceMap = new Dictionary<string, string>();
            //elementCounter = 0;
            //selectedElementId = "";
            OutlineItems = new List<OutlineItem>();
            cOutline.ClearContent();
            cBrowser.ClearContent();
            cEditor.Content = string.Empty;
            ReadCache = string.Empty;
            CurrentOpenFile = string.Empty;
            保存SToolStripMenuItem.Enabled = false;
            this.Text = "MD阅读器";
        }
        private void 打开OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            打开文件框.FileName = "*.md";
            打开文件框.DefaultExt = "md";
            if (打开文件框.ShowDialog() == DialogResult.OK)
            {
                ReadCache = string.Empty;
                // 获取用户选择的文件路径
                CurrentOpenFile = 打开文件框.FileName;
                OpenFile(CurrentOpenFile);
            }
        }

        private void frmReader_Load(object sender, EventArgs e)
        {
            // 恢复窗体位置和大小
            if (Properties.Settings.Default.FormLocation != Point.Empty)
            {
                var location = Properties.Settings.Default.FormLocation;
                var screen = Screen.FromPoint(location);
                if (screen.WorkingArea.Contains(location))
                {
                    this.Location = location;
                }
                else
                {
                    this.Location = new Point(100, 100); // 默认位置
                }
            }
            if (Properties.Settings.Default.FormSize != Size.Empty)
            {
                this.Size = Properties.Settings.Default.FormSize;
            }
            // 恢复窗体状态
            var windowState = Properties.Settings.Default.FormWindowState;
            this.WindowState = windowState;

            this.菜单.Enabled = false;

            #region 大纲的显示状态及位置
            if (Properties.Settings.Default.ShowOutlineForm)
                splitOutLine.Panel1Collapsed = false;
            #endregion

            if (string.IsNullOrEmpty(CurrentOpenFile))
                新建NToolStripMenuItem_Click(sender, e);

        }

        private void frmReader_FormClosing(object sender, FormClosingEventArgs e)
        {
            // 保存窗体位置和大小
            if (this.WindowState == FormWindowState.Normal)
            {
                Properties.Settings.Default.FormLocation = this.Location;
                Properties.Settings.Default.FormSize = this.Size;
            }
            else
            {
                Properties.Settings.Default.FormLocation = this.RestoreBounds.Location;
                Properties.Settings.Default.FormSize = this.RestoreBounds.Size;
            }
            // 保存窗体状态
            Properties.Settings.Default.FormWindowState = this.WindowState;
            // 保存设置
            Properties.Settings.Default.Save();
        }

        private void 大纲MToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!splitOutLine.Panel1Collapsed)
            {
                splitOutLine.Panel1Collapsed = true;
                // 保存窗体状态
                Properties.Settings.Default.ShowOutlineForm = false;
                // 保存设置
                Properties.Settings.Default.Save();
            }
            else
            {
                splitOutLine.Panel1Collapsed = false;
                // 保存窗体状态
                Properties.Settings.Default.ShowOutlineForm = true;
                // 保存设置
                Properties.Settings.Default.Save();
            }
        }

        private void cBrowser_InitalizedAfter(object sender, EventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                this.菜单.Enabled = true;
                tsslOpening.Visible = false;
                tspbOpening.Visible = false;
                this.tspbOpening.Style = ProgressBarStyle.Continuous;
                this.tspbOpening.MarqueeAnimationSpeed = 30; // 设置滚动速度，值越小速度越快
                this.tsslOpening.Text = "正在打开文件";

                if (!string.IsNullOrEmpty(ReadCache))
                    PutToRtf();
            }));
        }

        private void 设置SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MDReader.Control.frmSetting FrmSetting = new frmSetting();
            MDReader.Control.frmSetting FrmSetting = Program.ServiceProvider?.GetRequiredService<frmSetting>() ?? new frmSetting(logger, settingService);
            FrmSetting.ShowDialog();
        }

        private void cBrowser_ReceivedAfter(object sender, Microsoft.Web.WebView2.Core.CoreWebView2WebMessageReceivedEventArgs e)
        {
            if (!settingService.Get<bool>("源码随页面滚动"))
                return;
            if (isSyncingToBrowser) return;
            isSyncingToBrowser = true;
            isSyncingToTextBox = true;
            try
            {
                string headingId = e.TryGetWebMessageAsString();
                var outlineItem = OutlineItems.FirstOrDefault(item => item.Id == headingId);
                if (outlineItem != null)
                {
                    cEditor.ScrollToCaret(outlineItem);
                }
            }
            catch { }

            #region 仅最后一次真正进行处理
            ReceivedCounts++;
            int CurrentCount = ReceivedCounts;
            System.Threading.ThreadStart ts = new ThreadStart(new Action(() =>
            {
                System.Threading.Thread.Sleep(150);
                cBrowser.Invoke(() =>
                {
                    if (CurrentCount == ReceivedCounts)
                    {
                        ReceivedCounts = 0;
                        isSyncingToBrowser = false;
                    }
                });
            }));
            System.Threading.Thread t = new Thread(ts);
            t.IsBackground = true;
            t.Start();
            #endregion
            isSyncingToTextBox = false;
        }


        private void 修改CToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (splitEditor.Panel2Collapsed)
            {
                splitEditor.Panel2Collapsed = false;
                splitEditor.SplitterDistance = splitEditor.Width / 2;
            }
            else
            {
                splitEditor.Panel2Collapsed = true;
            }
        }

        private void cEditor_Accepted(object sender, EventArgs e)
        {
            if (cEditor.Content != ReadCache && MessageBox.Show("接受修改后，编辑区域将不能再撤回。确定要继续吗？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }
            ReadCache = cEditor.Content;
            PutToRtf();
        }

        private void cEditor_Canceled(object sender, EventArgs e)
        {
            cEditor.Content = ReadCache;
        }

        private void 保存SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cEditor.Content != ReadCache && MessageBox.Show("编辑的修改尚未被接受，确定要接受修改并保存吗？", "确认", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }
            ReadCache = cEditor.Content;
            PutToRtf();
            if (!string.IsNullOrEmpty(CurrentOpenFile))
            {
                try
                {
                    // 将ReadCache的内容写入CurrentOpenFile，覆盖文件内容
                    File.WriteAllText(CurrentOpenFile, ReadCache);
                    MessageBox.Show("文件保存成功！");
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"保存时遇到问题:\r\n{ex.Message}");
                }
            }
            else
            {
                保存文件框.FileName = "*.md";
                保存文件框.DefaultExt = "md";
                if (保存文件框.ShowDialog() == DialogResult.OK)
                {
                    File.WriteAllText(保存文件框.FileName, ReadCache);

                    string fileName = Path.GetFileName(保存文件框.FileName);
                    this.Text = $"MD阅读器      {fileName}";
                    CurrentOpenFile = 保存文件框.FileName;
                    MessageBox.Show("文件保存成功！");
                }
            }
        }

        private void 新建NToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OutlineItems = new List<OutlineItem>();
            cOutline.ClearContent();
            cBrowser.ClearContent();
            cEditor.Content = string.Empty;
            ReadCache = string.Empty;
            CurrentOpenFile = string.Empty;
            保存SToolStripMenuItem.Enabled = true;
            this.Text = "MD阅读器      新建-无标题.md";
            修改CToolStripMenuItem_Click(sender, e);
        }

        private void cEditor_SyncBrowserToOutline(object sender, MDReader.Control.CtrlEditor.SyncArgs e)
        {
            if (!settingService.Get<bool>("页面随源码滚动"))
                return;
            if (isSyncingToTextBox) return;
            isSyncingToBrowser = true;
            try
            {
                var outlineItem = this.OutlineItems.FirstOrDefault(item => item.LineNumber >= e.firstVisibleLine);
                if (outlineItem != null)
                {
                    cBrowser.JumpTo(outlineItem.Id.ToString());
                }
            }
            catch { }
            isSyncingToBrowser = false;
        }
        private void cEditor_Pasted(object sender, EventArgs e)
        {

            try
            {
                if (settingService.Get<bool>("粘贴表格时自动更新"))
                {
                    ReadCache = cEditor.Content;
                    PutToRtf();
                }
            }
            catch (Exception ex)
            { logger.WriteExceptionToEventLog(ex); }
        }

        private void cEditor_Toolstripped(object sender, EventArgs e)
        {

            try
            {
                if (settingService.Get<bool>("使用编辑工具后自动更新"))
                {
                    ReadCache = cEditor.Content;
                    PutToRtf();
                }
            }
            catch (Exception ex)
            { logger.WriteExceptionToEventLog(ex); }
        }
        #endregion
    }



}
