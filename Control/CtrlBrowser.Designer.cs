﻿namespace MDReader.Control
{
    partial class CtrlBrowser
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            wvContent = new Microsoft.Web.WebView2.WinForms.WebView2();
            pnlFind = new Panel();
            txtFind = new TextBox();
            pnllblfind = new Panel();
            lblFind = new Label();
            btnCloseFind = new Button();
            ((System.ComponentModel.ISupportInitialize)wvContent).BeginInit();
            pnlFind.SuspendLayout();
            pnllblfind.SuspendLayout();
            SuspendLayout();
            // 
            // wvContent
            // 
            wvContent.AllowExternalDrop = true;
            wvContent.CreationProperties = null;
            wvContent.DefaultBackgroundColor = Color.White;
            wvContent.Dock = DockStyle.Fill;
            wvContent.Location = new Point(0, 33);
            wvContent.Margin = new Padding(8);
            wvContent.Name = "wvContent";
            wvContent.Size = new Size(150, 117);
            wvContent.TabIndex = 7;
            wvContent.ZoomFactor = 1D;
            wvContent.NavigationCompleted += WvContent_NavigationCompleted;
            wvContent.WebMessageReceived += WebView_WebMessageReceived;
            // 
            // pnlFind
            // 
            pnlFind.Controls.Add(txtFind);
            pnlFind.Controls.Add(pnllblfind);
            pnlFind.Controls.Add(btnCloseFind);
            pnlFind.Dock = DockStyle.Top;
            pnlFind.Location = new Point(0, 0);
            pnlFind.Name = "pnlFind";
            pnlFind.Padding = new Padding(5);
            pnlFind.Size = new Size(150, 33);
            pnlFind.TabIndex = 8;
            pnlFind.Visible = false;
            // 
            // txtFind
            // 
            txtFind.Dock = DockStyle.Fill;
            txtFind.Location = new Point(52, 5);
            txtFind.Name = "txtFind";
            txtFind.Size = new Size(70, 23);
            txtFind.TabIndex = 0;
            txtFind.KeyDown += txtFind_KeyDown;
            // 
            // pnllblfind
            // 
            pnllblfind.Controls.Add(lblFind);
            pnllblfind.Dock = DockStyle.Left;
            pnllblfind.Location = new Point(5, 5);
            pnllblfind.Name = "pnllblfind";
            pnllblfind.Size = new Size(47, 23);
            pnllblfind.TabIndex = 3;
            // 
            // lblFind
            // 
            lblFind.AutoEllipsis = true;
            lblFind.AutoSize = true;
            lblFind.Font = new Font("Microsoft YaHei UI", 9F);
            lblFind.Location = new Point(5, 3);
            lblFind.Margin = new Padding(3, 3, 3, 0);
            lblFind.Name = "lblFind";
            lblFind.Size = new Size(35, 17);
            lblFind.TabIndex = 1;
            lblFind.Text = "查找:";
            // 
            // btnCloseFind
            // 
            btnCloseFind.Dock = DockStyle.Right;
            btnCloseFind.Location = new Point(122, 5);
            btnCloseFind.Name = "btnCloseFind";
            btnCloseFind.Size = new Size(23, 23);
            btnCloseFind.TabIndex = 2;
            btnCloseFind.Text = "X";
            btnCloseFind.UseVisualStyleBackColor = true;
            btnCloseFind.Click += btnCloseFind_Click;
            // 
            // CtrlBrowser
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(wvContent);
            Controls.Add(pnlFind);
            Name = "CtrlBrowser";
            Load += frmBrowser_Load;
            ((System.ComponentModel.ISupportInitialize)wvContent).EndInit();
            pnlFind.ResumeLayout(false);
            pnlFind.PerformLayout();
            pnllblfind.ResumeLayout(false);
            pnllblfind.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Microsoft.Web.WebView2.WinForms.WebView2 wvContent;
        private Panel pnlFind;
        private TextBox txtFind;
        private Panel pnllblfind;
        private Label lblFind;
        private Button btnCloseFind;
    }
}
