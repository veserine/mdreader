﻿namespace MDReader.Control
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlAction = new Panel();
            pnlSave = new Panel();
            btnSave = new Button();
            pnlCancel = new Panel();
            btnCancel = new Button();
            flowLayoutPanel1 = new FlowLayoutPanel();
            chkDefaultEdit = new CheckBox();
            panel1 = new Panel();
            pnlAction.SuspendLayout();
            pnlSave.SuspendLayout();
            pnlCancel.SuspendLayout();
            flowLayoutPanel1.SuspendLayout();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // pnlAction
            // 
            pnlAction.Controls.Add(pnlSave);
            pnlAction.Controls.Add(pnlCancel);
            pnlAction.Dock = DockStyle.Bottom;
            pnlAction.Location = new Point(0, 268);
            pnlAction.Name = "pnlAction";
            pnlAction.Size = new Size(450, 35);
            pnlAction.TabIndex = 0;
            // 
            // pnlSave
            // 
            pnlSave.Controls.Add(btnSave);
            pnlSave.Dock = DockStyle.Right;
            pnlSave.Location = new Point(290, 0);
            pnlSave.Name = "pnlSave";
            pnlSave.Padding = new Padding(5);
            pnlSave.Size = new Size(80, 35);
            pnlSave.TabIndex = 1;
            // 
            // btnSave
            // 
            btnSave.Dock = DockStyle.Fill;
            btnSave.Location = new Point(5, 5);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(70, 25);
            btnSave.TabIndex = 0;
            btnSave.Text = "保存(&S)";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = DockStyle.Right;
            pnlCancel.Location = new Point(370, 0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new Padding(5);
            pnlCancel.Size = new Size(80, 35);
            pnlCancel.TabIndex = 0;
            // 
            // btnCancel
            // 
            btnCancel.Dock = DockStyle.Fill;
            btnCancel.Location = new Point(5, 5);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(70, 25);
            btnCancel.TabIndex = 0;
            btnCancel.Text = "取消(&C)";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // flowLayoutPanel1
            // 
            flowLayoutPanel1.Controls.Add(panel1);
            flowLayoutPanel1.Dock = DockStyle.Fill;
            flowLayoutPanel1.Location = new Point(0, 0);
            flowLayoutPanel1.Name = "flowLayoutPanel1";
            flowLayoutPanel1.Size = new Size(450, 268);
            flowLayoutPanel1.TabIndex = 1;
            // 
            // chkDefaultEdit
            // 
            chkDefaultEdit.AutoSize = true;
            chkDefaultEdit.Dock = DockStyle.Fill;
            chkDefaultEdit.Location = new Point(5, 5);
            chkDefaultEdit.Name = "chkDefaultEdit";
            chkDefaultEdit.Size = new Size(210, 25);
            chkDefaultEdit.TabIndex = 0;
            chkDefaultEdit.Text = "设置为.md文件默认编辑器";
            chkDefaultEdit.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            panel1.Controls.Add(chkDefaultEdit);
            panel1.Location = new Point(3, 3);
            panel1.Name = "panel1";
            panel1.Padding = new Padding(5);
            panel1.Size = new Size(220, 35);
            panel1.TabIndex = 1;
            // 
            // frmSetting
            // 
            AcceptButton = btnSave;
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new Size(450, 303);
            Controls.Add(flowLayoutPanel1);
            Controls.Add(pnlAction);
            Name = "frmSetting";
            Text = "设置";
            Load += frmSetting_Load;
            pnlAction.ResumeLayout(false);
            pnlSave.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            flowLayoutPanel1.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel pnlAction;
        private Panel pnlCancel;
        private Button btnCancel;
        private Panel pnlSave;
        private Button btnSave;
        private FlowLayoutPanel flowLayoutPanel1;
        private CheckBox chkDefaultEdit;
        private Panel panel1;
    }
}