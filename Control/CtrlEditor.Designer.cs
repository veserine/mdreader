﻿namespace MDReader.Control
{
    partial class CtrlEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pnlAction = new Panel();
            pnlAccept = new Panel();
            btnAccept = new Button();
            pnlCancel = new Panel();
            btnCancel = new Button();
            txtSource = new RichTextBox();
            toolStripContainer1 = new ToolStripContainer();
            toolStrip1 = new ToolStrip();
            pnlAction.SuspendLayout();
            pnlAccept.SuspendLayout();
            pnlCancel.SuspendLayout();
            toolStripContainer1.ContentPanel.SuspendLayout();
            toolStripContainer1.TopToolStripPanel.SuspendLayout();
            toolStripContainer1.SuspendLayout();
            SuspendLayout();
            // 
            // pnlAction
            // 
            pnlAction.Controls.Add(pnlAccept);
            pnlAction.Controls.Add(pnlCancel);
            pnlAction.Dock = DockStyle.Bottom;
            pnlAction.Location = new Point(0, 397);
            pnlAction.Name = "pnlAction";
            pnlAction.Size = new Size(555, 35);
            pnlAction.TabIndex = 0;
            // 
            // pnlAccept
            // 
            pnlAccept.Controls.Add(btnAccept);
            pnlAccept.Dock = DockStyle.Right;
            pnlAccept.Location = new Point(395, 0);
            pnlAccept.Name = "pnlAccept";
            pnlAccept.Padding = new Padding(5);
            pnlAccept.Size = new Size(80, 35);
            pnlAccept.TabIndex = 0;
            // 
            // btnAccept
            // 
            btnAccept.Dock = DockStyle.Fill;
            btnAccept.Location = new Point(5, 5);
            btnAccept.Name = "btnAccept";
            btnAccept.Size = new Size(70, 25);
            btnAccept.TabIndex = 0;
            btnAccept.Text = "生效(&A)";
            btnAccept.UseVisualStyleBackColor = true;
            btnAccept.Click += btnAccept_Click;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = DockStyle.Right;
            pnlCancel.Location = new Point(475, 0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new Padding(5);
            pnlCancel.Size = new Size(80, 35);
            pnlCancel.TabIndex = 1;
            // 
            // btnCancel
            // 
            btnCancel.Dock = DockStyle.Fill;
            btnCancel.Location = new Point(5, 5);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(70, 25);
            btnCancel.TabIndex = 0;
            btnCancel.Text = "取消(&C)";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // txtSource
            // 
            txtSource.AcceptsTab = true;
            txtSource.DetectUrls = false;
            txtSource.Dock = DockStyle.Fill;
            txtSource.Location = new Point(0, 0);
            txtSource.Name = "txtSource";
            txtSource.Size = new Size(555, 372);
            txtSource.TabIndex = 1;
            txtSource.Text = "";
            txtSource.WordWrap = false;
            txtSource.VScroll += TxtSource_VScroll;
            txtSource.MultilineChanged += txtSource_MultilineChanged;
            txtSource.TextChanged += txtSource_TextChanged;
            txtSource.KeyDown += txtSource_KeyDown;
            txtSource.Validated += txtSource_Validated;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            toolStripContainer1.ContentPanel.Controls.Add(txtSource);
            toolStripContainer1.ContentPanel.Size = new Size(555, 372);
            toolStripContainer1.Dock = DockStyle.Fill;
            toolStripContainer1.Location = new Point(0, 0);
            toolStripContainer1.Name = "toolStripContainer1";
            toolStripContainer1.Size = new Size(555, 397);
            toolStripContainer1.TabIndex = 2;
            toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            toolStripContainer1.TopToolStripPanel.Controls.Add(toolStrip1);
            // 
            // toolStrip1
            // 
            toolStrip1.Dock = DockStyle.None;
            toolStrip1.Location = new Point(3, 0);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.Size = new Size(43, 25);
            toolStrip1.TabIndex = 0;
            // 
            // CtrlEditor
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(toolStripContainer1);
            Controls.Add(pnlAction);
            Name = "CtrlEditor";
            Size = new Size(555, 432);
            pnlAction.ResumeLayout(false);
            pnlAccept.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            toolStripContainer1.ContentPanel.ResumeLayout(false);
            toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            toolStripContainer1.TopToolStripPanel.PerformLayout();
            toolStripContainer1.ResumeLayout(false);
            toolStripContainer1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel pnlAction;
        private Panel pnlAccept;
        private Button btnAccept;
        private Panel pnlCancel;
        private Button btnCancel;
        private RichTextBox txtSource;
        private ToolStripContainer toolStripContainer1;
        private ToolStrip toolStrip1;
    }
}