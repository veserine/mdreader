using Markdig.Helpers;
using MDReader.Control;
using MDReader.Helper;
using MDReader.Script.Settings;
using MDReader.Script.Toolstrips;
using MDReader.Script.Toolstrips.Base;
using MDReader.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Reflection;
namespace MDReader
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            //string filePath = "";
            //if (args != null)
            //    filePath = args.Length > 0 ? args[0] : "";
            LogService log = new LogService();
            // 配置DI容器
            var serviceCollection = new ServiceCollection();
            {
                // 注册ILog和LogHelper
                serviceCollection.AddSingleton<ILog, LogService>(provider => log);
                serviceCollection.AddSingleton<IMainArgs, MainArgsService>(provider => new MainArgsService(args));
                serviceCollection.AddSingleton<IConfigurationService, ConfigurationService>();
                serviceCollection.AddSingleton<ISettingService, SettingService>();
                serviceCollection.AddSingleton<IToolstripService, ToolstripService>();

                // 注册你的窗体
                serviceCollection.AddTransient<frmReader>();
                serviceCollection.AddTransient<frmSetting>();
                serviceCollection.AddTransient<set_001_SyncBrowser>();
                serviceCollection.AddTransient<set_000_SyncEditor>();
                serviceCollection.AddTransient<set_002_AutoUpdateAfterPasteTable>();
                serviceCollection.AddTransient<set_003_AutoUpdateAfterToolstrip>();
                serviceCollection.AddTransient<tool_011_Italic>();
                serviceCollection.AddTransient<tool_010_Bold>();
                serviceCollection.AddTransient<tool_000_AddLevel>();
                serviceCollection.AddTransient<tool_001_MinLevel>();
                serviceCollection.AddTransient<tool_003_AddBlock>();
                serviceCollection.AddTransient<tool_002_MinBlock>();
                serviceCollection.AddTransient<tool_020_AddLine>();
                serviceCollection.AddTransient<tool_021_AddCode>();
                serviceCollection.AddTransient<tool_022_AddTable>();
                serviceCollection.AddTransient<tool_030_DateStamp>();
                serviceCollection.AddTransient<tool_031_TimeStamp>();
            }
            ServiceProvider = serviceCollection.BuildServiceProvider(); // 容器生效

            // 注册设置项
            {
                ServiceProvider.GetRequiredService<ISettingService>().Register(ServiceProvider);
                ServiceProvider.GetRequiredService<IToolstripService>().Register(ServiceProvider);
            }

            //try
            //{
            //    throw new Exception("试试抛出个异常");
            //}
            //catch (Exception ex)
            //{
            //    log.WriteExceptionToEventLog(ex);
            //}

            // 启动主窗体
            try
            {
                var mainForm = ServiceProvider.GetRequiredService<frmReader>();
                Application.Run(mainForm);
            }
            catch (Exception ex)
            {
                MessageBox.Show("遇到知名错误，不得不关闭程序。\r\n可以通过事件查看器了解进一步的信息。" , "致命错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                log.WriteExceptionToEventLog(ex);
            }
            //Application.Run(new frmReader(filePath));
            //Application.Run(new frmReader());
        }
        public static ServiceProvider? ServiceProvider { get; private set; } 

    }
}