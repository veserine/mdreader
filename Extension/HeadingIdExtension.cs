﻿using Markdig.Renderers;
using Markdig.Syntax;
using Markdig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDReader.Extension
{

    public class HeadingIdExtension : IMarkdownExtension
    {
        public void Setup(MarkdownPipelineBuilder pipeline)
        {
            pipeline.DocumentProcessed += DocumentProcessed;
        }
        public void Setup(MarkdownPipeline pipeline, IMarkdownRenderer renderer)
        {
        }
        private void DocumentProcessed(MarkdownDocument document)
        {
            int headingCount = 0;
            foreach (var node in document.Descendants<HeadingBlock>())
            {
                if (node is HeadingBlock heading && heading.Inline != null)
                {
                    heading.SetData("id", $"heading-{headingCount}");
                    headingCount++;
                }
            }
        }
    }

}
